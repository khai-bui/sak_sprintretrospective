const env = require('dotenv').config({ path: `${__dirname}/.env` });
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const dbRtns = require('../routines/dbroutines');
mongoose.Promise = global.Promise;

// Home page
router.get('/', (req, res) => {
	res.send(req.session.user);
})

// Retrieves array of projects under current user
router.get('/projects', async (req, res) => {
	let conn;
	let projects = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getProjectsFromMember(req.session.user);
		
		if (result !== null) {
			for (let i = 0; i < result.projects.length; i++)
				projects.push(result.projects[i].name);
		}		
		res.send(projects);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
})

// Retrieves array of projects under a user
router.get('/members/:name/projects', async (req, res) => {
	let name = req.params.name;
	let projects = [];
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getProjectsFromMember(name);
		for (let i = 0; i < result.projects.length; i++)
			projects.push(result.projects[i].name);
		res.send(projects);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves full json for a team
router.get('/projects/:name', async (req, res) => {
	let name = req.params.name;
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.findProject(name);
		res.send(result);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves non-current user stories
router.get('/icebox', async (req, res) => {
	let conn;
	let icebox = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getIceBox(req.query.project);
		
		for (let i = 0; i < result.stories.length; i++) {
			icebox.push(result.stories[i].story);
		}
		res.send(icebox);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves current user stories
router.get('/backlog', async (req, res) => {
	let conn;
	let backlog = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getBacklog(req.query.project);
		for (let i = 0; i < result.stories.length; i++) {
			backlog.push(result.stories[i].story);
		}
		res.send(backlog);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves completed user stories
router.get('/done', async (req, res) => {
	let conn;
	let done = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getCompleteStories(req.query.project);
		for (let i = 0; i < result.stories.length; i++) {
			done.push(result.stories[i].story);
		}
		res.send(done);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
})

// Retrieves an array of members under a project
router.get('/members', async (req, res) => {
	let conn;
	let members = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getMembersInProject(req.query.project);		
		for (let i = 0; i < result.members.length; i++)
			members.push(result.members[i].name);
		res.send(members);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves json of stories under a member under a project
router.get('/projects/:project/members/:member/stories', async (req, res) => {
	let project = req.params.project;
	let member = req.params.member;
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getStoriesFromMember(project, member);		
		res.send(result.members[0].stories);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.get('/story', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.findStory(req.query.project, req.query.story);
		res.send(result);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
})

// Retrieves json of stories under a current user
router.get('/stories', async (req, res) => {
	let conn;
	let stories = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getStoriesFromMember(req.query.project, req.session.user);

		for (let i = 0; i < result.members[0].stories.length; i++)
			stories.push(result.members[0].stories[i].story);
		
		res.send(stories);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.get('/storiesFull', async (req, res) => {
	let conn;
	let stories = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getStoriesFromMember(req.query.project, req.session.user);
		res.send(result.members[0].stories);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves json of stories under a current user
router.get('/sprints', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getStoriesFromProject(req.query.project);
		res.send(result);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves information on the PMO
router.get('/pmo', async (req, res) => {
	let conn;
	let pmo = '';
	let members = [];
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getPMO(req.query.project);
		for (let i = 0; i < result.members.length; i++)
			pmo = result.pmo === result.members[i].name ? result.members[i] : 'No PMO found';
		res.send(pmo);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

// Retrieves current logged in user's member id
router.get('/myID', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getMemberFromUser(req.session.user);
		res.send(result._id);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.get('/memberId', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getMemberFromUser(req.query.name);
		res.send(result._id);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.get('/storyId', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getStoryID(req.query.story, req.query.project);
		res.send(result.stories[0]._id);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.post('/project', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let body = {name: req.body.name, pmo: req.session.user, members: [JSON.parse(req.body.id)] };
		let docsAdded = await dbRtns.addProject(body);
		res.send(docsAdded);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('insert project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.post('/story', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let docsAdded = await dbRtns.addStory(req.body);
		res.send(docsAdded);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('insert story failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/projectToMember', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let body = {name: req.session.user, projectId: req.body.projectId};
		let project = await dbRtns.addProjectToMember(body);
		res.send(project)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update member failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/member', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let member = await dbRtns.addMemberToProject(req.body);
		res.send(member)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/storyToMember', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let story = await dbRtns.addStoryToMember(req.body);
		res.send(story)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/storyToProject', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let story = await dbRtns.addStoryToProject(req.body);
		res.send(story)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/time', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let time = await dbRtns.addTimeToStory(req.body);
		res.send(time)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/startSprint', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let time = await dbRtns.startStory(req.body);
		res.send(time)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.put('/finishSprint', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let time = await dbRtns.finishStory(req.body);
		res.send(time)
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('update project failed - internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

module.exports = router;