const env = require('dotenv').config({ path: `${__dirname}/.env` });
const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const dbRtns = require('../routines/dbroutines');
mongoose.Promise = global.Promise;

let auth = (req, res, next) => {
	if (req.session && req.session.user !== null && req.session.user !== '')
		return next();
	else
		return res.send('error');
};

// Home page
router.get('/', (req, res) => {
	res.send(`You got home page!\n`);
})

router.get('/users', async (req, res) => {
	let users = [];
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getUsers();
		for (let i = 0; i < result.length; i++)
			users.push(result[i].username);
		res.send(users);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

router.get('/login', async (req, res) => {
	let conn;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let result = await dbRtns.getUser(req.query.username, req.query.password);
		if (result !== null && result !== '') {
			req.session.user = result.username;
			req.session.save();
			res.send(true);
		}
		else
			res.send(false);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
})

// Logout endpoint
router.get('/logout', (req, res) => {
	req.session.destroy();
	res.send("logout success!");
});

router.post('/register', async (req, res) => {
	let conn;
	let name = req.body.username;
	try {
		conn = await mongoose.connect(
			process.env.DBURL,
			{ useNewUrlParser: true }
		);
		const db = mongoose.connection;
		let docsAdded = await dbRtns.addUser(req.body);	
		let memJSON = {name: name, user: mongoose.Types.ObjectId(docsAdded._id)};
		let memAdded = await dbRtns.addMember(memJSON);
		res.send(docsAdded);
		res.send(memAdded);
	} catch (err) {
		console.log(err.stack);
		res.status(500).send('internal server error');
	} finally {
		if (conn) conn.disconnect();
	}
});

 
module.exports = router;