const express = require('express');
const router = express.Router();

router.use('/sprint', require('./sprint'));
router.use('/auth', require('./auth'));

module.exports = router;