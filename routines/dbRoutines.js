const model = require('../models/sprintModels');

const findProject = (name) => model.Project.findOne({ name: name }).populate({
	path: 'members',
	model: model.Member,
	select: '-projects'
}).populate({
	path: 'stories',
	model: model.Story
}).lean();

const getUsers = () => model.User.find({}).select('username -_id');;

const getUser = (name, pass) => model.User.findOne({username: name, password: pass});

const getMemberFromUser = (user) => model.Member.findOne({name: user});

const getStoryID = (story, project) => model.Project.findOne({name: project}).populate({
	path: 'stories',
	model: model.Story,
	match: { story: story }
});

const getProjectsFromMember = (name) => model.Member.findOne({name: name}).populate({
	path: 'projects',
	model: model.Project,
	select: 'name -_id'
}).select('projects -_id').lean();

const getIceBox = (name) => model.Project.findOne({name: name}).populate({
	path: 'stories',
	model: model.Story,
	match: { current: false, complete: false }
}).select('stories -_id').lean();

const getBacklog = (name) => model.Project.findOne({name: name}).populate({
	path: 'stories',
	model: model.Story,
	match: { current: true, complete: false }
}).select('stories -_id').lean();

const getCompleteStories = (name) => model.Project.findOne({name: name}).populate({
	path: 'stories',
	model: model.Story,
	match: { complete: true }
}).select('stories -_id').lean();

const getMembersInProject = (name) => model.Project.findOne({name: name}).populate({
	path: 'members',
	model: model.Member,
	select: 'name -_id'
}).select('members -_id');

const getStoriesFromMember = (project, member) => model.Project.findOne({name: project}).populate({
	path: 'members',
	model: model.Member,
	match: { name: member },
	populate: {
		path: 'stories',
		model: model.Story,
		match: { project: project}
	},
	select: 'stories -_id'
}).select('members -_id').lean();

const getStoriesFromProject = (project) => model.Project.findOne({name: project}).populate({
	path: 'members',
	model: model.Member,
	populate: {
		path: 'stories',
		model: model.Story,
		match: { project: project}
	},
	select: 'stories name -_id'
}).select('members -_id').lean();

const getPMO = (project) => model.Project.findOne({name: project}).populate({
	path: 'members',
	model: model.Member,
	populate: {
		path: 'stories',
		model: model.Story,
		match: { project: project}
	}, 
	select: '-projects'
}).select('members pmo').lean();

const findStory = (project, story) => model.Story.findOne({story: story, project: project});

const addUser = (doc) => model.User.create(doc);

const addMember = (doc) => model.Member.create(doc);

const addStory = (doc) => model.Story.create(doc);

const addProject = (doc) => model.Project.create(doc);

const addMemberToProject = (doc) => model.Project.findOneAndUpdate({
	name: doc.projectName,
	$addToSet: { members: doc.memberId }
});

const addProjectToMember = (doc) => model.Member.findOneAndUpdate({
	name: doc.name,
	$addToSet: { projects: doc.projectId }
});

const addStoryToMember = (doc) => model.Member.findOneAndUpdate({
	id: doc.id,
	$addToSet: { stories: doc.storyId }
});

const addTimeToStory = (doc) => model.Story.findOneAndUpdate(
	{story: doc.story, project: doc.project},
	{$inc: {actualtime: doc.hours}, $set: {actualpoints: doc.actualpoints}}
);

const startStory = (doc) => model.Story.findOneAndUpdate(
	{story: doc.story, project: doc.project},
	{$set: {current: true}}
);

const finishStory = (doc) => model.Story.findOneAndUpdate(
	{story: doc.story, project: doc.project, current: true, complete: false},
	{$set: {complete: true, current: false}}
);

const addStoryToProject = (doc) => model.Project.findOneAndUpdate({
	name: doc.ProjectName,
	$addToSet: { stories: doc.storyId }
});

module.exports = { 
	findProject, getMemberFromUser, getProjectsFromMember, getUser, getUsers, getIceBox, getBacklog, getCompleteStories, getMembersInProject, getStoriesFromMember, getPMO, addMemberToProject, addStoryToMember, addUser, addMember, addStory, addProject, getStoriesFromProject, addTimeToStory, addProjectToMember, addStoryToProject, getStoryID, startStory, finishStory, findStory
}