const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectModelSchema = new Schema({
	name: String,
	pmo: String,
	members: [{
		type: Schema.Types.ObjectId,
		ref: 'Member'
	}],
	stories: [{
		type: Schema.Types.ObjectId,
		ref: 'Story'
	}]
});

const MemberModelSchema = new Schema({
	name: String,
	stories: [{
		type: Schema.Types.ObjectId,
		ref: 'Story'
	}],
	projects: [{
		type: Schema.Types.ObjectId,
		ref: 'Project'
	}],
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	}
})

const StoryModelSchema = new Schema({
	story: String,
	sprint: Number,
	points: Number,
	project: String,
	estimatetime: Number,
	current: Boolean,
	complete: Boolean,
	actualtime: Number,
	actualpoints: Number
})

const UserModelSchema = new Schema({
	username: String,
	password: String
})

// Compile model from schema
const Project = mongoose.model('Project', ProjectModelSchema, process.env.PROJECTCOLLECTION);
const Member = mongoose.model('Member', MemberModelSchema, process.env.MEMBERCOLLECTION);
const Story = mongoose.model('Story', StoryModelSchema, process.env.STORYCOLLECTION);
const User = mongoose.model('User', UserModelSchema, process.env.USERCOLLECTION);

module.exports = { Project, Member, Story, User }