const env = require('dotenv').config({ path: `${__dirname}/.env` });
const express = require('express');
const session = require('express-session');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 5000;

app.use((req, res, next) => {
	console.log('Time:', new Date() + 3600000 * - 5.0); // GMT-->EST
	next();
});

// cors
app.use((req, res, next) => {
	res.header('access-control-allow-origin', 'http://localhost:3000');
	res.header(
		'access-control-allow-headers',
		'origin, x-requested-with, content-type, accept'
	);
	res.header('Access-Control-Allow-Credentials', 'true');
	res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
	next();
});

app.use(session({
	secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

// parse application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'))

app.use((err, req, res, next) => {
	// Do logging and user-friendly error message display
	console.error(err);
	res.status(500).send('internal server error');
});

app.use(require('./routes'));

app.listen(port, () => {
	console.log(`listening on port ${port} - ${process.env.NODE_ENV}`);
});